#include <stdio.h>
#include <stdlib.h>
int main()
{
    
    int x=1;
    int *p= &x;
    
    printf("%d\n",p); //Devolve endereço
    
    //printf("%d\n",*p); //Devolve valor da variável
    
    printf("%d\n",*(p+1)); //Devolve conteudo de (Posição de endereço+1) Muda o endereço
    printf("%d\n",*p+1);  //Devolve conteudo de P + 1
    
    printf("%d\n",p+1);   //Devolve endereço
    
   // printf("%d\n",*p+1); //Devolve (valor_da_variável+1)
        
        *p = x*5;//Faz 1*5 e guarda no endereço p
    //    printf("%d\n",p); //printa o endereço guardado na variável P
    //    printf("%d\n",*p);//printa o conteudo do endereço guardado em P
    return 0;
}